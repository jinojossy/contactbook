.. contactbook documentation master file, created by
   sphinx-quickstart on Sun Jan 28 21:25:54 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

technocore contactbook's documentation
=======================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   installation
   contactbook
   settings
   developer
