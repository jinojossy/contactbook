.. Document the application settings.

Settings
########
The settings can be changed from the home screen using the settings command::

    :setting <name> <action> [option]


Themes
------
The application has several themes to choose from. To list all themes::

    :setting theme list

To change the theme use::

    :setting theme set <theme name>

WARNING: For the changes to take place the application must be restarted.

About
-----
The about screen can be opened using the about command::

    :about
