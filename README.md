# README  

A TUI contact book for the terminal, written in Python 3.

Fetures:
* Terminal application with a TUI.
* Import/Export contacts using the vCard format.
* Search.
* Cross platform.
* Open source.
* Color themes.
* Data stored in a database.

Source Code at: https://gitlab.com/thomas1814/contactbook  
Documentation at: http://contactbook.readthedocs.io/en/latest/  

### Installation
The technocore contactbook can be installed using pip:
```$ pip install technocore --user```

To start the application, open a terminal and enter:
```$ technocore```

If you receive an error about technocore command not found, you will most likely have to add it to your PATH vaiable. This will depend on you system, but typically the command is located under “/Python/bin/.”. Once added to PATH, relaunch the terminal.

To uninstall:
```$ pip uninstall technocore```

### Usage
Once the application is started, the home screen is blank. Bring up the contact book with: ```:contacts``` This will open the contact book. Full documentation can be found at the documentation: http://contactbook.readthedocs.io/en/latest/
